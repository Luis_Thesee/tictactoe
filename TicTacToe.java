import java.util.Scanner;
public class TicTacToe{
	public static void main (String[] args){
		Scanner s = new Scanner(System.in);
		boolean gameOver = false;
		int player = 1;
		int row;
		int col;
		Square playerToken = Square.X;
		Board board = new Board();
		
		while (!gameOver){
			System.out.print(board);
			if (player == 1){
				playerToken = Square.X;
			}
			else{
				playerToken = Square.O;
			}
		
			System.out.println("Player "+ player + " input the row where you would like to put your token");
			row = s.nextInt();
			
			System.out.println("Player "+ player + " input the column where you would like to put your token");
			col = s.nextInt();
			
			while(!board.placeToken(row, col, playerToken)){
				System.out.println("This place is already taken, please enter valid numbers");
				System.out.println("Player "+ player + " re-input the row where you would like to put your token");
				row = s.nextInt();
			
				System.out.println("Player "+ player + " re-input the column where you would like to put your token");
				col = s.nextInt();
			}
			
			if (board.checkIfFull()){
				System.out.println("It's a tie");
				gameOver = true;
			}
			else if (board.checkIfWinning(playerToken)){
				System.out.println(board);
				System.out.println("Player " + player + " is the winner!!!!");
				gameOver = true;
			}
			else {
				player = player + 1;
				if(player > 2){
					player = 1;
				}
			}
				
	}
	}
}