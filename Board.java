public class Board{
	private Square[][] tictatoeBoard;
	
	public Board(){
		this.tictatoeBoard = new Square[3][3];
		for (int i = 0; i < this.tictatoeBoard.length; i++){
			for (int j = 0; j < this.tictatoeBoard[i].length;  j++){
				this.tictatoeBoard[i][j] = Square.BLANK;
			}
		}
	}
	
	public boolean placeToken(int row, int col, Square playerToken){
		if (row > 3 && col > 3 && row < 0 && col < 0){
			return false;
		}
		else{
			this.tictatoeBoard[row][col] = playerToken;
			return true;
		}
	}
	
	public boolean checkIfFull(){
		for (int i = 0; i < this.tictatoeBoard.length; i++){
			for (int j = 0; j < this.tictatoeBoard[i].length;  j++){
					if(this.tictatoeBoard[i][j] == Square.BLANK){
						return false;
					}
			}
		}
		return true;
	}
	
	private boolean checkIfWinningHorizontal(Square playerToken){
		int count = 0;
		for (int i = 0; i < 3; i++){
			for (int j = 0; j < 3; j++){
				if (this.tictatoeBoard[i][j] == playerToken){
					count++;
				}
			}
			if (count == 3){
				return true;
			}
		}
		return false;
	}			
	
	private boolean checkIfWinningVertical(Square playerToken){
		int count = 0;
		for (int i = 0; i < 3; i++){
			for (int j = 0; j < 3; j++){
				if (this.tictatoeBoard[j][i] == playerToken){
					count++;
				}
			}
			if (count == 3){
				return true;
			}
		}
		return false;
	}			
	
	public boolean checkIfWinning(Square playerToken){
		if(checkIfWinningHorizontal(playerToken) || checkIfWinningVertical(playerToken)){
			return true;
		}
		else{
			return false;
		}
	}

	public String toString(){
		String board = "";
		for (int i = 0; i < this.tictatoeBoard.length; i++){
			for (int j = 0; j < this.tictatoeBoard[i].length;  j++){
				board += this.tictatoeBoard[i][j] + " ";
			}
			board += "\n";
		}
		return board;
	}
}